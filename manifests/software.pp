# software build from source
class sms::software
(
  String $user = 'user',
  String $userdir = '/home/user',
  )

{
  tag 'slow', 'building_software', 'sms'
  require put::buster




  $pkgs = [
    'python3-pocketsphinx',
    'python-pocketsphinx',
    'pocketsphinx-en-us',
    'pocketsphinx',
    'gstreamer1.0-pocketsphinx',
    'python-gst-1.0',
    'python3-gst-1.0',
    'gstreamer1.0-espeak',
    'moc',
    #'ibniz',
    #'lives',
    #'lives-plugins',
    'ffmpeg',
    'build-essential',
    # cannot find it'checkinstall',
    'cmake',
    'libasound2-dev',
    'libavcodec-dev',
    'libavdevice-dev',
    'libavfilter-dev',
    'libavformat-dev',
    'libavutil-dev',
    'libcurl4-openssl-dev',
    'libfdk-aac-dev',
    'libfontconfig-dev',
    'libfreetype6-dev',
    'libgl1-mesa-dev',
    'libjack-jackd2-dev',
    'libjansson-dev',
    'libluajit-5.1-dev',
    'libpulse-dev',
    'libqt5x11extras5-dev',
    'libqt5multimedia5',
    'lua5.3',
    'libspeexdsp-dev',
    'libswresample-dev',
    'libswscale-dev',
    'libudev-dev',
    'libv4l-dev',
    'libvlc-dev',
    'libx11-dev',
    'libx264-dev',
    'libxcb-shm0-dev',
    'libxcb-xinerama0-dev',
    'libxcomposite-dev',
    'libxinerama-dev',
    'pkg-config',
    'python3-dev',
    'qtbase5-dev',
    'libqt5svg5-dev',
    'swig',
    'qjackctl',
    'ardour',
    'swh-lv2',
    'tap-plugins',
    'fil-plugins',
    'calf-plugins',
    'x42-plugins',
    #'liquidsoap-plugin-jack',
    'jack-tools',
    'ninja-build',
    'meson',
    'libgstreamer1.0-dev',
    'libgstreamer-plugins-bad1.0-dev',
    'libgstreamer-plugins-base1.0-dev',
    'libgstrtspserver-1.0-dev',
    'gstreamer1.0-plugins-good',
    'gstreamer1.0-plugins-ugly',
    'libqt5gstreamerutils-1.0-0',
    'gstreamer1.0-tools',
    'libass-dev',
    'libfreetype6-dev',
    'libsdl2-dev',
    'libtool',
    'libva-dev',
    'libvdpau-dev',
    'libvorbis-dev',
    'libxcb1-dev',
    'libxcb-shm0-dev',
    'libxcb-xfixes0-dev',
    'pkg-config',
    'texinfo',
    'wget',
    'zlib1g-dev',
    'nasm',
    'libx264-dev',
    'libx265-dev',
    'libnuma-dev',
    'libvpx-dev',
    'libfdk-aac-dev',
    'libmp3lame-dev',
    'libopus-dev',
    'avahi-utils',
    'libgstreamer1.0-dev',
    'libgstreamer-plugins-base1.0-dev',
    'gstreamer1.0-plugins-base',
    'gstreamer1.0-plugins-good',
    'gstreamer1.0-plugins-bad',
    'gstreamer1.0-plugins-ugly',
    'gstreamer1.0-libav',
    'libgstrtspserver-1.0-dev',
    'cargo',
  #nageru
    'libqt5printsupport5',
    'libsdl2-image-dev',
    'libbmusb-dev',
    'libbmusb5',
    'libeigen3-dev',
    'libepoxy-dev',
    'libgcrypt20-dev',
    'libgmp-dev',
    'libgmpxx4ldbl',
    #'libgnutls-dane0',
    #'libgnutls-openssl27',
    #'libgnutls28-dev',
    #'libgnutlsxx28',
    'libgpg-error-dev',
    'libidn11-dev',
    'liblua5.2-0',
    'liblua5.2-dev',
    'liblua5.3-dev',
    'liblua5.1-dev',
    'liblua5.1-0-dev',
    'liblua50-dev',
    #'libmicrohttpd-dev',
    #'libmicrohttpd12',
    'libmovit-dev',
    'libmovit8',
    'libp11-kit-dev',
    'libpci-dev',
    'libprotobuf-dev',
    'libprotobuf-lite17',
    'libprotobuf17',
    'libprotoc17',
    'libreadline-dev',
    'libtasn1-6-dev',
    'libunbound8',
    'libunbound-dev',
    'libzita-resampler-dev',
    'libzita-resampler1',
    'nettle-dev',
    'protobuf-compiler',
    'googletest',
    'libgtest-dev',
    'libjpeg62-turbo-dev',
    'libjpeg-dev',
    'libsqlite3-dev',
    'libqcustomplot-dev',
    #'nageru',
    #'futatabi',
    #editing:
    #'kdenlive',
    'ebumeter',
    'python-pip',
    'python-pip',
    'libbmusb-dev',
    'libbmusb5',
    'qt5-default',
    'libusb-1.0-0-dev',
    'libusb-1.0-doc',
    'npm',
    #mumble
    'libavahi-client-dev',
    'libavahi-common-dev',
    'libavahi-compat-libdnssd-dev',
    'libcap-dev',
    'libclang1-7',
    'libmcpp0',
    'libqt5designer5',
    'libqt5designercomponents5',
    'libqt5help5',
    'libspeechd-dev',
    'libzeroc-ice-dev',
    'libzeroc-ice3.7',
    'qdoc-qt5',
    'qt5-assistant',
    'qttools5-dev-tools',
    'zeroc-ice-compilers',
    'zeroc-ice-slice',
    'qt5-default',
    'qttools5-dev-tools',
    'libsndfile1-dev',
    'libsndfile1',
    'libboost-all-dev',

  ]

  ensure_packages($pkgs, {ensure => 'installed'})

  include put::nodejs


  Exec {
    user    => 'root',
    cwd     => '/root/',
    timeout => 9999,
    path    => '/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin',
    environment => ['HOME=/root' ]
  }

#  exec { 'backports meson':
#    command => 'apt-get -t stretch-backports install meson',
#    cwd     => "${srcdir}/"
#  }


  $srcdir = '/usr/local/src'
  #49152 to 65535. 
#  put::buildfromgit { 'linux-firewire-utils':
#    repo     => 'https://github.com/cladisch/linux-firewire-utils.git',
#    commands => ['autoreconf -i', 'configure', 'make', 'make install'],
#  }

  #exec { 'cmake from backports':
  # command   => 'apt-get install -t buster-backports cmake',
  #} ->
  put::buildfromgit { 'mumble':
    repo     => 'https://github.com/mumble-voip/mumble.git',
    tag      => ['mumble'],
    revision  => '1.3.3',
    cwd       => './build',
    commands => [
    'qmake CONFIG+="no-g15 no-embed-qt-translations no-server no-dbus no-pulseaudio no-oss no-alsa no-gkey " -recursive ..',
#     "cmake -Dalsa=OFF -Dpulseaudio=OFF -Dserver=OFF  ..", 
     'make', 
    ],

    # qmake CONFIG+="no-g15 no-server no-dbus no-pulseaudio no-oss no-alsa no-gkey " -recursive
  }





  #FIXME: make buildfromgit build stuff not from git
  $ndilib_installer = 'libndi-linux-v3.8.0-install.sh'
#  $ndilib_installer = 'InstallNDISDK_v4_Linux.sh'

  file { "${srcdir}/libndi":
    ensure => directory,
  }
  -> file { "${srcdir}/libndi/${ndilib_installer}":
    ensure => present,
    #owner  => root,
    #group  => root,
    #mode   => '0700',
    source => "puppet:///modules/${module_name}/${ndilib_installer}",
  }
  -> exec { 'ndilib rm':
    command   => 'rm -fr "NDI SDK for Linux"',
    cwd       => "${srcdir}/libndi",
    subscribe => File["${srcdir}/libndi/${ndilib_installer}"],
#FIXME    refreshonly => true,

  }
  -> exec { 'ndilib unpack':
    command   => "yes | PAGER=cat sh ${ndilib_installer}",
    cwd       => "${srcdir}/libndi",
    subscribe => File["${srcdir}/libndi/${ndilib_installer}"],
#FIXME    refreshonly => true,


  }
  -> exec { 'ndilib rm nd':
    command   => 'rm -fr ndisdk',
    cwd       => "${srcdir}/libndi",
    subscribe => File["${srcdir}/libndi/${ndilib_installer}"],
#    refreshonly => true,


  }
  -> exec { 'ndilib mv':
    command   => 'mv -f "NDI SDK for Linux" ndisdk',
    cwd       => "${srcdir}/libndi",
    subscribe => File["${srcdir}/libndi/${ndilib_installer}"],
    #creates => "${srcdir}/libndi",
#    refreshonly => true,
  }
  exec { 'ndilib  inst':
    command => 'cp lib/x86_64-linux-gnu/libndi.so* /usr/lib/',
    cwd     => "${srcdir}/libndi/ndisdk",
    #   subscribe => File["${srcdir}/libndi/${ndilib_installer}"],
    #    refreshonly => true,
  }
  -> exec { 'ndilib dev inst':
    command => 'cp include/Processing.NDI.* /usr/include/',
    cwd     => "${srcdir}/libndi/ndisdk",
    #   subscribe => File["${srcdir}/libndi/${ndilib_installer}"],
    #    refreshonly => true,
  }
  -> exec { 'ndilib ldconfig':
    command => 'ldconfig',
    cwd     => "${srcdir}/libndi/ndisdk",
    #   subscribe => File["${srcdir}/libndi/${ndilib_installer}"],
    #    refreshonly => true,
  }


  put::buildfromgit { 'obs-studio':
    repo_name => 'obs-studio',
    tag       => ['obsbuild'],
    revision  => '25.0.7',
    #srcdir => "/usr/local/src",
    repo      => 'https://github.com/obsproject/obs-studio.git',
    cwd       => './build',
    commands  => ['cmake -DUNIX_STRUCTURE=1 -DCMAKE_INSTALL_PREFIX=/usr ..', 'make', 'make install', 'ldconfig'],
  }

  put::buildfromgit { 'obs-gstreamer':
    repo     => 'https://github.com/fzwoch/obs-gstreamer.git',
    commands => ['mkdir -p build', 'meson --buildtype=release --prefix /usr build', 'ninja -C build', 'ninja -C build install'],
  }

  put::buildfromgit { 'obs-v4l2sink':
    repo     => 'https://github.com/CatxFish/obs-v4l2sink.git',
    commands => ['mkdir -p build', "cmake -DLIBOBS_INCLUDE_DIR=${srcdir}/obs-studio/libobs -DCMAKE_INSTALL_PREFIX=/usr", 'make', 'make install'],
  }



  put::buildfromgit { 'obs-ndi':
    repo     => 'https://github.com/Palakis/obs-ndi.git',
    cwd      => 'build',
    revision => '5b8a7f8577eb2bb205699916a24ef9cc38deffd6',
    commands => [ "cmake -DLIBOBS_INCLUDE_DIR=${srcdir}/obs-studio/libobs -DCMAKE_INSTALL_PREFIX=/usr ..", 'make', 'make install '],
  }

  put::buildfromgit { 'obs-websocket':
    repo     => 'https://github.com/Palakis/obs-websocket.git',
    cwd      => 'build',
    revision => '4.7.0',
    commands => [ "cmake -DLIBOBS_INCLUDE_DIR=${srcdir}/obs-studio/libobs -DCMAKE_INSTALL_PREFIX=/usr ..", 'make', 'make install'] ,
  }

  put::buildfromgit { 'obs-dir-watch-media':
    repo     => 'https://github.com/exeldro/obs-dir-watch-media.git',
    cwd      => 'build',
    #revision => '4.7.0',
    commands => [ "cmake -DLIBOBS_INCLUDE_DIR=${srcdir}/obs-studio/libobs -DCMAKE_INSTALL_PREFIX=/usr ..", 'make', 'make install'] ,
  }

  put::buildfromgit { 'obs-rtspserver':
    repo     => 'https://github.com/iamscottxu/obs-rtspserver.git',
    cwd      => 'build',
    #revision => '4.7.0',
    commands => [ "cmake -DLIBOBS_INCLUDE_DIR=${srcdir}/obs-studio/libobs -DCMAKE_INSTALL_PREFIX=/usr ..", 'make', 'make install'] ,
  }

  put::buildfromgit { 'obs-vnc':
    repo     => 'https://github.com/norihiro/obs-vnc.git',
    cwd      => 'build',
    #revision => '4.7.0',
    commands => [ "cmake -DLIBOBS_INCLUDE_DIR=${srcdir}/obs-studio/libobs -DCMAKE_INSTALL_PREFIX=/usr ..", 'make', 'make install'] ,
  }

  put::buildfromgit { 'obs-multi-rtmp':
    repo     => 'https://github.com/sorayuki/obs-multi-rtmp.git',
    cwd      => 'build',
    #revision => '4.7.0',
    commands => [ "cmake -DLIBOBS_INCLUDE_DIR=${srcdir}/obs-studio/libobs -DCMAKE_INSTALL_PREFIX=/usr ..", 'make', 'make install'] ,
  }

  put::buildfromgit { 'obs-gphoto':
    repo     => 'https://github.com/Atterratio/obs-gphoto.git',
    cwd      => 'build',
    #revision => '4.7.0',
    commands => [ "cmake -DLIBOBS_INCLUDE_DIR=${srcdir}/obs-studio/libobs -DCMAKE_INSTALL_PREFIX=/usr ..", 'make', 'make install'] ,
  }






#  put::buildfromgit { 'obs-facemask':
#    repo     => 'https://github.com/stream-labs/facemask-plugin.git',
#    cwd      => 'build',
#    revision => 'master',
#    commands => [ "cmake -DLIBOBS_INCLUDE_DIR=${srcdir}/obs-studio/libobs -DPATH_OBS_STUDIO=${srcdir}/obs-studio/ -DCMAKE_INSTALL_PREFIX=/opt ..", 'make', 'make install'],
##cmake -DPATH_OBS_STUDIO=/usr/local/src/obs-studio/  -DCMAKE_INSTALL_PREFIX=/opt -DPATH_DLIB=../thirdparty/dlib/ ..
##cmake -DPATH_OBS_STUDIO=/usr/local/src/obs-studio/  -DCMAKE_INSTALL_PREFIX=/opt -DPATH_DLIB=../thirdparty/dlib/ -DPATH_FREETYPE=../thirdparty/freetype2  ..
#    #debiandeps => ['libdlib-dev', 'libopenblas-dev', 'liblapack-dev', 'nvidia-cuda-dev'],
#  }

  put::buildfromgit { 'obs-tablet-remote':
    repo     => 'https://github.com/t2t2/obs-tablet-remote.git',
    commands => ['npm install', 'npm run build -- --homepage /'],
  }
#  put::buildfromgit { 'pipewire':
#    repo     => 'https://github.com/PipeWire/pipewire.git',
#    cwd      => 'build',
#    commands => ['autogen.sh', 'make'],
#  }




#ffmpeg
#cd ~/ffmpeg_sources && wget -O ffmpeg-snapshot.tar.bz2 https://ffmpeg.org/releases/ffmpeg-snapshot.tar.bz2 && tar xjvf ffmpeg-snapshot.tar.bz2 && cd ffmpeg && PATH="$HOME/bin:$PATH" PKG_CONFIG_PATH="$HOME/ffmpeg_build/lib/pkgconfig" ./configure --prefix="$HOME/ffmpeg_build" --pkg-config-flags="--static" --extra-cflags="-I$HOME/ffmpeg_build/include -I$HOME/ffmpeg_sources/ndi/include" --extra-ldflags="-L$HOME/ffmpeg_build/lib -L$HOME/ffmpeg_sources/ndi/lib/x86_64-linux-gnu" --extra-libs="-lpthread -lm" --bindir="$HOME/bin" --enable-gpl --enable-libass --enable-libfreetype --enable-libvorbis --enable-libx264 --enable-libx265 --enable-libndi_newtek --enable-nonfree && PATH="$HOME/bin:$PATH" 
#make -j4 && make install && hash -r

  put::buildfromgit { 'ffmpeg-ndi':
      repo     => 'https://github.com/FFmpeg/FFmpeg.git',
      revision => '6b5ea90eace8f822ea158d4d15e0d0214d1a511e',

      commands => [
        'bash configure --prefix=/srv/ffmpeg --extra-libs="-lpthread -lm" --enable-libvorbis --enable-libx264 --enable-libx265 --enable-nonfree --enable-gpl --enable-libass --enable-libfreetype --enable-libndi_newtek   --enable-libfdk-aac --enable-libopus --enable-libfdk-aac ',
        'make -j2',
        'make install'
        ],

  }


}

