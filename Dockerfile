FROM debian:buster

RUN apt update
RUN apt install -y puppet screen vim git

WORKDIR /etc/puppet/code/modules
RUN mkdir sms


#WORKDIR /etc/puppet/code/modules/sms

RUN git clone https://git.puscii.nl/puppetexp/put.git
RUN git clone https://github.com/puppetlabs/puppetlabs-apt apt
RUN git clone https://github.com/puppetlabs/puppetlabs-stdlib stdlib
RUN git clone https://github.com/puppetlabs/puppetlabs-firewall firewall
RUN git clone https://github.com/puppetlabs/puppetlabs-git git
RUn git clone https://github.com/puppetlabs/puppetlabs-vcsrepo vcsrepo


ADD . /etc/puppet/code/modules/sms
RUN puppet module list --tree

RUN puppet apply --modulepath=/etc/puppet/code/modules/ -e "include sms::streamingservernd"

RUN apt-get -y install nginx libnginx-mod-rtmp
RUN mkdir -p /opt/data/hls
CMD nginx
