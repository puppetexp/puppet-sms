  vcsrepo { "${srcdir}/movit":
    ensure   => present,
    provider => git,
    #  revision => '6b5ea90eace8f822ea158d4d15e0d0214d1a511e',
    source   => 'http://git.sesse.net/movit',
  }
  -> exec { 'movit autogen':
    command     => 'bash autogen.sh',
    cwd         => "${srcdir}/movit",
    subscribe   => Vcsrepo["${srcdir}/movit"],
    refreshonly => true,

  }
  -> exec { 'movit make':
    command     => 'make',
    cwd         => "${srcdir}/movit",
    subscribe   => Vcsrepo["${srcdir}/movit"],
    refreshonly => true,
  }
  -> exec { 'movit make install':
    command     => 'make install',
    cwd         => "${srcdir}/movit",
    subscribe   => Vcsrepo["${srcdir}/movit"],
    refreshonly => true,
  }

# $cefname = 'cef_binary_73.1.12+gee4b49f+chromium-73.0.3683.75_linux64_minimal.tar.bz2'
# $cefurl =  'http://opensource.spotify.com/cefbuilds/cef_binary_73.1.12%2Bgee4b49f%2Bchromium-73.0.3683.75_linux64_minimal.tar.bz2'

  $cefurl = 'http://opensource.spotify.com/cefbuilds/cef_binary_77.1.8%2Bg41b180d%2Bchromium-77.0.3865.90_linux32_minimal.tar.bz2'
  $cefname ='cef_binary_77.1.8+g41b180d+chromium-77.0.3865.90_linux32_minimal.tar.bz2'

  file {"${srcdir}/cef":
    ensure => directory
  }
  -> exec {'fetch cef':
    command => "wget -O ${cefname} ${cefurl}" ,
    cwd     => "${srcdir}/cef",
    creates => "${srcdir}/cef/${cefname}"
  }
  -> exec {'unpack cef':
    command => "tar -xf ${cefname}",
    cwd     => "${srcdir}/cef"
  }



  vcsrepo { "${srcdir}/nageru":
    ensure   => present,
    provider => git,
    #  revision => '6b5ea90eace8f822ea158d4d15e0d0214d1a511e',
    source   => 'http://git.sesse.net/nageru',
  }
  -> exec { 'nageru meson':
    #    command => 'meson obj -Dcef_dir=/usr/lib/x86_64-linux-gnu/cef Dcef_build_type=system -Dcef_no_icudtl=true',
    command     => "meson obj -Dcef_dir=${srcdir}/cef/${cefname} -Dcef_no_icudtl=true",
    cwd         => "${srcdir}/nageru",
    subscribe   => Vcsrepo["${srcdir}/nageru"],
    refreshonly => true,

  }
  -> exec { 'nageru ninja':
    command     => 'ninja',
    cwd         => "${srcdir}/nageru/obj",
    subscribe   => Vcsrepo["${srcdir}/nageru"],
    refreshonly => true,

  }


