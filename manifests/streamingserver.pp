# sms audio
class sms::streamingserver (
  String $docker_repo = 'https://git.puscii.nl/sms/streaming-docker.git',
  String $docker_repo_revision = 'master',
  String $icecast_password = 'hackme',
  String $liquid_file = 'playlist.liq',
  String $streaming_media_stuff_path = '/usr/local/src/streaming-media-stuff',
  String $input_password = 'hackme',
  String $test = 'whatever'
  )
  {

  tag 'sms'

  # this means that first the buster class will be applied (so we have our apt repo's etc.)
  require put::buster

  ## this are the defaults for the exec resource (cause it starts with a capital)
  Exec {
    user    => 'root',
    cwd     => '/root/',
    timeout => 9999,
    path    => '/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin',
    environment => ['HOME=/root' ]
  }

  file{ '/root/sources':
    ensure => directory,
    owner  => 'root'
  }

  $file_contents = @("EOF")
contents or our testfile
the password is: ${icecast_password}
EOF

  $liquidsoapunit = @("EOF")
[Unit]
Description=Liquidsoap
[Service]
Environment=SMS_ICECAST_PASSWORD=${icecast_password}
Environment=SMS_INPUT_PASSWORD=${input_password}
ExecStart=/usr/bin/liquidsoap ${streaming_media_stuff_path}/liquidsoap/${liquid_file}
[Install]
WantedBy=multi-user.target
EOF

  $testdaemon = @("EOF")
[Unit]
Description=Testing
[Service]
Environment=SMS_TEST=${test}
ExecStart=${streaming_media_stuff_path}/testdaemon.sh
[Install]
WantedBy=multi-user.target
EOF

file{ '/etc/systemd/system/test.service':
    ensure  => present,
    content => $testdaemon,
}
file{ '/root/testfile':
    ensure  => present,
    content => $file_contents,
  }

file{ '/etc/systemd/system/liquidsoapsms.service':
    ensure  => present,
    content => $liquidsoapunit,
  }

exec { 'daemon-reload-liquidsoap':
  command => 'systemctl daemon-reload'
}


  # enable the firewall
  include put::firewall

  # some exceptions to the firewall
  firewall { '901 rtmp':
    dport  => '1935',
    proto  => 'tcp',
    action => 'accept',
  }

  firewall { '22 ssh':
    dport  => '22',
    proto  => 'tcp',
    action => 'accept',
  }




  # install some packages
  $pkgs = [
    'docker',
    'liquidsoap',
  ]

  ensure_packages($pkgs, {ensure => 'installed'})

  # clone the streaming-docker repo
  vcsrepo { '/root/sources/streaming-docker':
    ensure   => present,
    provider => git,
    owner    => root,
    group    => root,
    remote   => 'origin',
    user     => root,
    revision => $docker_repo_revision,
    source   => {
      'origin' => $docker_repo,
    }
  }


  vcsrepo { $streaming_media_stuff_path:
    ensure   => present,
    provider => git,
    owner    => root,
    group    => root,
    remote   => 'origin',
    revision => 'master',
    user     => root,
    source   => {
      'origin' => 'https://git.puscii.nl/sms/streaming-media-stuff.git',
    }
  }

  file { '/mnt/radio':
    ensure => directory,
    owner  => 'root',

  }
}
