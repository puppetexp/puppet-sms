# kxstudio test
class sms::kxstudio (
  Boolean $enable_extra = false,
  )
  {


  apt::key { 'kxstudio key':
    id     => 'DF1BC724E4ED8A947FF0B0A1F8599E482BD84BD9',
    server => 'pgp.mit.edu',
  }
  -> apt::source { 'kxstudio_plugins':
    ensure   => absent,
    comment  => 'kxstudio plugins',
    location => 'http://ppa.launchpad.net/kxstudio-debian/plugins/ubuntu',
    release  => 'bionic',
    repos    => 'main',
    pin      => -1000,
    include  => {
      'src' => true,
      'deb' => true,
    },
  }

  if $enable_extra {
    notify { 'extra kx stuff enabled (will break your system)':
      }
    #FIXME: kxstudio has no source packages, and so fuckes up ardour build
    apt::source { 'kxstudio_apps':
      ensure   => absent,
      comment  => 'kxstudio_apps',
      location => 'http://ppa.launchpad.net/kxstudio-debian/apps/ubuntu',
      release  => 'bionic',
      repos    => 'main',
      pin      => -1000,
      include  => {
        'src' => true,
        'deb' => true,
      },
    }



  } else {

    #FIXME: kxstudio has no source packages, and so fuckes up ardour build
    apt::source { 'kxstudio_apps':
      ensure   => absent,
      comment  => 'kxstudio_apps',
      location => 'http://ppa.launchpad.net/kxstudio-debian/apps/ubuntu',
      release  => 'bionic',
      repos    => 'main',
      pin      => -1000,
      include  => {
        'src' => true,
        'deb' => true,
      },
    }

  }

}
