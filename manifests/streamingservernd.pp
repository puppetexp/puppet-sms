# sms audio
class sms::streamingservernd (
  String $icecast_password = 'hackme',
  String $liquid_file = 'playlist.liq',
  String $streaming_media_stuff_path = '/usr/local/src/streaming-media-stuff',
  String $input_password = 'hackme',
  String $test = 'whatever'
  )
  {

  tag 'sms'

  # this means that first the buster class will be applied (so we have our apt repo's etc.)
  require put::buster

  
  
  # enable the firewall
 # include put::firewall

  # some exceptions to the firewall
 # firewall { '901 rtmp':
 #   dport  => '1935',
 #   proto  => 'tcp',
 #   action => 'accept',
 # }

  #firewall { '22 ssh':
  #  dport  => '22',
  #  proto  => 'tcp',
  #  action => 'accept',
  #}


  # install some packages
  $pkgs = [
    'libnginx-mod-rtmp',
    'nginx'
  ]

  ensure_packages($pkgs, {ensure => 'installed'})
 
  file { '/etc/nginx/sites-enabled/default':
    ensure => absent
  }
  
  #exec { 'delete file':
  #  command     => '/bin/bash -c "rm /etc/nginx/sites-available/default ; true"',
  #}


  file { '/mnt/radio':
    ensure => directory,
    owner  => 'root',
  }

  file { '/mnt/radio/hls':
    ensure => directory,
    owner  => 'www-data',

  }

  file { '/mnt/radio/record':
    ensure => directory,
    owner  => 'www-data',

  }





  $nginxrtmp_conf = {
    hostname    => $hostname,
    record_path => '/mnt/radio/record',
    hls_path    =>  '/mnt/radio/hls'
  }

  file { "/etc/nginx/modules-enabled/rtmp.conf":
    content => epp("${module_name}/nginx-rtmp-rtmp.conf.epp", $nginxrtmp_conf),
    #notify => Service['nginx'],
  }

  file { '/etc/nginx/sites-enabled/rtmp-http.conf':
    content => epp("${module_name}/nginx-rtmp-http.conf.epp", $nginxrtmp_conf),
    #notify  => Service['nginx'],
  }

 }
