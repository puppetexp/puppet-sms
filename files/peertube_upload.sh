#!/usr/bin/env bash

## DEPENDENCIES: httpie, jq
# pip install httpie
USERNAME="test"
PASSWORD="test123"
FILE_PATH="$1"
CHANNEL_ID="testchannel"
NAME=`basename $FILE_PATH`
HOST="testpeertube.laglab.org"

API_PATH="https://${HOST}/api/v1"

## AUTH
client_id=$(http -b GET "$API_PATH/oauth-clients/local" | jq -r ".client_id")
client_secret=$(http -b GET "$API_PATH/oauth-clients/local" | jq -r ".client_secret")
token=$(http -b --form POST "$API_PATH/users/token" \
  client_id="$client_id" client_secret="$client_secret" grant_type=password response_type=code \
  username=$USERNAME \
  password=$PASSWORD \
  | jq -r ".access_token")

## VIDEO UPLOAD
http -b --form POST "$API_PATH/videos/upload" \
  videofile@"$FILE_PATH" \
  channelId=$CHANNEL_ID \
  name="$NAME" \
  "Authorization:Bearer $token"
