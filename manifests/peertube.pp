# peertube
class sms::peertube (
  String  $glwritetoken = 'no_token_set',
  String  $ircprefix    = 'irc-prefix-not-set',
  String  $user         = 'peertube',
  String  $ptdir        = '/var/www/peertube',
  String  $ptversion    = 'v2.0.0',
  String  $ptdbname     = 'peertube_prod',
  String  $hostname     = 'testpeertube.laglab.org',
  String  $admin_email  = 'peertube@eduif.nl',
  Boolean $dev          = false,
  )
  {

  # TODO:
  # * vpn for outgoing traffic (following other instances will make request to them)
  # * proxy in front
  Exec {
    user    => 'root',
    cwd     => '/root/',
    timeout => 9999,
    path    => '/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin',
    environment => ['HOME=/root' ]
  }

  include nginx
#  class{"nginx":
#    manage_repo => true,
#    #package_source => 'nginx-mainline'
#    dynamic_modules => ['ngx_rtmp_module'],
#    nginx_cfg_prepend => { 'include' => '/etc/nginx/modconf.d/*.conf', },
#  }

  firewall { '99 nginx port 80':
    dport  => '80',
    proto  => 'tcp',
    action => 'accept',
  }
  firewall { '99 nginx port 443':
    dport  => '443',
    proto  => 'tcp',
    action => 'accept',
  }
  firewall { '99 nginx backend port 8443':
    dport  => '8443',
    proto  => 'tcp',
    action => 'accept',
  }
  firewall { '99 nginx backend port 8080':
    dport  => '8080',
    proto  => 'tcp',
    action => 'accept',
  }
  firewall { '99 apache backend port 9443':
    dport  => '9443',
    proto  => 'tcp',
    action => 'accept',
  }
  firewall { '99 apache backend port 9080':
    dport  => '9080',
    proto  => 'tcp',
    action => 'accept',
  }
  firewall { '99 apache cache port 3443':
    dport  => '3443',
    proto  => 'tcp',
    action => 'accept',
  }
  firewall { '99 apache cache 3080':
    dport  => '3080',
    proto  => 'tcp',
    action => 'accept',
  }


  include put::ttor
  tor::daemon::onion_service { 'onion_nginx_80':
    ports => 80;
  }



  include put::postgresqlserver
  include sms::nginxrtmp
  include git

  $pt_conf = {
    hostname        => $hostname,
    db_database     => 'peertube',
    db_user         => 'peertube',
    db_password     => Deferred('put_getsecret', ['postgres/peertube']),
    email_host      => 'meel.puscii.nl',
    email_user      => 'peertube@eduif.nl',
    email_password  =>  Deferred('put_getsecret', ['peertube/smtp_pass']),
    email_from      => 'peertube@eduif.nl',
    admin_email     => $admin_email,
  }
  $nginx_conf = {
    hostname    => $hostname,
    admin_email => $admin_email,
  }
  file { '/var/cache/peertube':
    ensure => directory,
    owner  => 'www-data',
  }
  file { '/var/cache/peertube/videos':
    ensure => directory,
    owner  => 'www-data',
  }

  file { "/etc/nginx/sites-enabled/${hostname}.conf":
    content => epp("${module_name}/peertube-nginx.conf.epp", $nginx_conf),
    notify  => Service['nginx'],
  }
  file { "/etc/nginx/sites-enabled/${hostname}.cache.conf":
    content => epp("${module_name}/peertube-nginx-cache.conf.epp", $nginx_conf),
    notify  => Service['nginx'],
  }


  class { 'apache':
    default_vhost => false,
  }


  apache::listen { '3080': }
  apache::listen { '3443': }

  apache::listen { '9080': }
  apache::listen { '9443': }
  class { 'apache::mod::ssl': }
  class { 'apache::mod::proxy': }
  class { 'apache::mod::proxy_balancer': }
  #class { 'apache::mod::proxy_http': }
  class { 'apache::mod::proxy_wstunnel': }
  class { 'apache::mod::rewrite': }
  class { 'apache::mod::cache': }
  class { 'apache::mod::disk_cache': }
  class { 'apache::mod::headers': }



#  apache::mod { 'mod_ssl': }
#  apache::mod { 'mod_proxy': }
#  apache::mod { 'mod_proxy_balancer': }
#  apache::mod { 'proxy_http': }
  apache::mod { 'lbmethod_byrequests': }
  apache::mod { 'lbmethod_bybusyness': }
  apache::mod { 'lbmethod_bytraffic': }


  file { "/etc/apache2/sites-enabled/${hostname}.conf":
    content => epp("${module_name}/peertube-apache.conf.epp", $nginx_conf),
    notify  => Service['apache2'],
  }

  file { "/etc/apache2/sites-enabled/${hostname}-cache.conf":
    content => epp("${module_name}/peertube-apache-cache.conf.epp", $nginx_conf),
    notify  => Service['apache2'],
  }



  $psql_pass = Deferred('put_getsecret', ['postgres/peertube'])

  postgresql::server::db { $ptdbname:
    user     => 'peertube',
    #password => Deferred('postgresql_password', [$ptdbname, $psql_pass]),
    #FIXME: doesn work with deferred:
    #Error: Could not retrieve catalog from remote server: Error 500 on SERVER: Server Error: Evaluation Error: Error while evaluating a Resource Statement, Evaluation Error
    #: Left match operand must result in a String value. Got an Object. (file: /etc/puppet/code/environments/production/modules/postgresql/manifests/server/role.pp, line: 12
    #7, column: 10) (file: /etc/puppet/code/environments/production/modules/postgresql/manifests/server/db.pp, line: 41) on node fresh.lag

    password => postgresql_password('peertube', '3d7894bf9d2ff812dfa60605094fc6a6'),

  }
  -> exec{ 'create unaccent extension':
    user    => 'postgres',
    command => "psql -c 'CREATE EXTENSION unaccent;' ${ptdbname}",
  }
  -> exec{ 'create pg_trgm extension':
    user    => 'postgres',
    command => "psql -c 'CREATE EXTENSION pg_trgm;' ${ptdbname}",
  }

#  postgresql::server::db { 'peertubetest':
#    user     => 'peertubetest',
#    password => Deferred('postgresql_password', [$ptdbname, $psql_pass]),
#
#  }

  exec{ 'set psql password':
    user    => 'postgres',
#    command => Deferred('sprintf', ["psql -c 'ALTER USER peertube WITH PASSWORD ${psql_pass};' ${ptdbname}"]),
#    Error: Failed to apply catalog: Function sprintf not defined despite being loaded!

#    command => "psql -c 'ALTER USER peertube WITH PASSWORD 3d7894bf9d2ff812dfa60605094fc6a6;' ${ptdbname}"
#    command => "psql -c \"ALTER USER peertube WITH PASSWORD '3d7894bf9d2ff812dfa60605094fc6a6';\" ${ptdbname}"
    command => 'psql -c "ALTER USER peertube WITH PASSWORD \'3d7894bf9d2ff812dfa60605094fc6a6\';" peertube_prod',
  }




  $pkgs = [
    'ffmpeg',
    'postgresql-contrib',
    'openssl',
    'g++',
    'make',
    'redis-server',
    'python-dev'
  ]
  ensure_packages($pkgs, {ensure => 'installed'})


  file { $ptdir:
    ensure => directory,
    owner  => $user,
  }

  user { $user:
    system => true,
    home   => $ptdir,
  }

  include put::nodejs

  $nodepkgs = [
    'yarn',
    'typescript',
    'supertest',
    'chai'
  ]
  ensure_packages( $nodepkgs,
  {
    ensure   => latest,
    provider => 'npm',
    install_options => [ '-g' ],
  })

  #$ cd /var/www/peertube && sudo -u peertube mkdir config storage versions && cd versions

  file { "${ptdir}/config":
    ensure => directory,
    owner  => $user
  }
  file { "${ptdir}/storage":
    ensure => directory,
    owner  => $user
  }
  file { "${ptdir}/versions":
    ensure => directory,
    owner  => $user
  }
  file { "${ptdir}/zips":
    ensure => directory,
    owner  => $user
  }

  $pt_template = file("${module_name}/peertube-production.yaml.epp")

#  sudo -u peertube wget -q "https://github.com/Chocobozzz/PeerTube/releases/download/${VERSION}/peertube-${VERSION}.zip"
#$ sudo -u peertube unzip peertube-${VERSION}.zip && sudo -u peertube rm peertube-${VERSION}.zip 
#$ SQL_BACKUP_PATH="backup/sql-peertube_prod-$(date -Im).bak" && \
#    cd /var/www/peertube && sudo -u peertube mkdir -p backup && \
#    sudo -u postgres pg_dump -F c peertube_prod | sudo -u peertube tee "$SQL_BACKUP_PATH" >/dev/null 
  archive { "peertube-${ptversion}.zip":
    path         => "/var/www/peertube/zips/peertube-${ptversion}.zip",
    source       => "https://github.com/Chocobozzz/PeerTube/releases/download/${ptversion}/peertube-${ptversion}.zip",
    extract      => true,
    extract_path => "${ptdir}/versions",
    creates      => "${ptdir}/versions/peertube-${ptversion}",
    cleanup      => true,
    user         => $user,
    #require      => File['wso2_appdir'],
  }
  -> vcsrepo { '/var/www/peertube/versions/peertube-develop':
    ensure     => present,
    provider   => git,
    revision   => 'develop',
    source     => 'https://github.com/whohoho/PeerTube.git',
    submodules => true,
    owner      => $user,
  }
  -> file { "${ptdir}/versions/peertube-latest":
    ensure => 'link',
    target => "${ptdir}/versions/peertube-develop",
  }
  if $dev == true {
    # if NODE_ENV is set to production when doing npm install, it will not install build dependencies
    exec{ 'peertube npm client install':
      command     => 'npm install',
      user        => $user,
      cwd         => "${ptdir}/versions/peertube-develop/client",
      environment => ["HOME=${ptdir}" ]
    }
    -> exec{ 'peertube npm install':
      command     => 'npm install',
      user        => $user,
      cwd         => "${ptdir}/versions/peertube-develop",
      environment => ["HOME=${ptdir}" ]
    }
    -> exec{ 'peertube npm build:server':
      command     => 'npm run-script build',
      user        => $user,
      cwd         => "${ptdir}/versions/peertube-develop",
      environment => ["HOME=${ptdir}" ]
    }
    -> exec{ 'peertube npm build:server':
      command     => 'npm run build:server',
      user        => $user,
      cwd         => "${ptdir}/versions/peertube-develop",
      environment => ["HOME=${ptdir}" ]
    }
  }
  exec{ 'peertube yarn install':
    command     => 'yarn install --production --pure-lockfile',
    user        => $user,
    cwd         => "${ptdir}/versions/peertube-${ptversion}",
    environment => ["HOME=${ptdir}" ]
  }
  -> file { "${ptdir}/config/production.yaml":
    ensure  => file,
    content => Deferred('inline_epp', [$pt_template + { $listen_port => '9000' }, $pt_conf]),
    notify  => Service['peertube@production']
  }
  -> file { "${ptdir}/config/production4.yaml":
    ensure  => file,
    content => Deferred('inline_epp', [$pt_template + { $listen_port => '9004' }, $pt_conf]),
#    notify => Service['peertube@production4']
  }
  -> file { "${ptdir}/config/production5.yaml":
    ensure  => file,
    content => Deferred('inline_epp', [$pt_template + { $listen_port => '9005' }, $pt_conf]),
#    notify => Service['peertube@production5']
  }

#  $ cd ../ && sudo -u peertube ln -s versions/peertube-${VERSION} ./peertube-latest
#  $ cd ./peertube-latest && sudo -H -u peertube yarn install --production --pure-lockfile

  #exec { 'pt copy unit file':
  #  command => "cp ${ptdir}/versions/peertube-latest/support/systemd/peertube.service /etc/systemd/system/"
  #} -> 
  file { '/etc/systemd/system/peertube.service':
    source => "puppet:///modules/${module_name}/peertube.service",
  }
  -> exec { 'pt daemon-reload':
    command => 'systemctl daemon-reload'
  }
  -> exec { 'pt start 1':
    command => 'systemctl restart peertube@production.service'
  }
  -> exec { 'pt start 2':
    command => 'systemctl restart peertube@production2.service'
  }
  -> exec { 'pt start 3':
    command => 'systemctl restart peertube@production3.service'
  }
  -> exec { 'pt restart':
    command => 'systemctl restart peertube@production.service'
  }
  -> exec { 'pt status':
    command => 'systemctl status peertube@production'
  }

  service { 'peertube@production':
    ensure => 'running',
    enable => true,
  }
  service { 'peertube@production2':
    ensure => 'running',
    enable => true,
  }
  service { 'peertube@production3':
    ensure => 'running',
    enable => true,
  }

}
