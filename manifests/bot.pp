# sms audio
class sms::bot (
  String  $glwritetoken = 'no_token_set',
  String  $ircprefix  = 'irc-prefix-not-set',
  String $user = 'user',
  Boolean $enable = true,
  )
  {
  tag 'sms'
  require put::buster


  $py2pkgs = [
    'Mastodon.py',
  ]
  ensure_packages( $py2pkgs,
  {
    ensure   => present,
    provider => 'pip',
  })

  Exec {
    user    => 'root',
    cwd     => '/root/',
    timeout => 9999,
    path    => '/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin',
    environment => ['HOME=/root' ]
  }


  exec { 'pip uninstall -y sopel':
    command => 'pip uninstall -y sopel',
  }
  exec { 'pip3 uninstall -y sopel':
    command => 'pip3 uninstall -y sopel',
  }



  exec { 'install sopel':
    command => 'pip install sopel',
  }

#  package { 'sopel_python3':
#    name => 'sopel',
#    ensure   => absent,
#    provider => 'pip3',
#  }

#  package { 'sopel_python2':
#    name => 'sopel',
#    ensure   => latest,
#    provider => 'pip',
#  }


#  ensure_packages( ['sopel'], {ensure => absent, provider => 'pip'})

# fixme stupid stuff again with same name differen package provider
#  $pypkgs = [
#    'Mastodon.py'
#  ]
#  ensure_packages( $pypkgs,
#  {
#    ensure   => present,
#    provider => 'pip',
#  })





  exec { 'disble sopel systemservice (when exist)':
    command  => 'systemctl disable sopel || true',
    provider => 'shell',
  }

  $sopel_indy = @("EOF")
[core]
nick = bot_${ircprefix}_${hostname}
host = irc.indymedia.org
#host = irc.hackint.org
use_ssl = true
port = 6697
owner = nobody
channels = #lag
#logdir=/var/log/sopel
#pid_dir=/run/sopel
homedir=/home/user/src/streaming-media-stuff/sopel
enable = shoutout,version,route


[url]
exclude = 
exclusion_char = !

[admin]
hold_ground = true
auto_accept_invite = true

[safety]
enabled_by_default = false
known_good = 

[clock]
tz = UTC
time_format = %Y-%m-%d - %T%Z

[shout]
db = /home/user/src/streaming-media-stuff/leftover/leftover.db

[mastodon]
access_token = /home/user/src/streaming-media-stuff/leftover/mast/pytooter_usercred.secret
api_base_url = https://social.weho.st

[mattermost]
username = test
password = test
EOF
  file{ "/home/${user}/.puppet/sopel_indymedia.cfg":
    content => $sopel_indy,
  }

  $sopel_ifmchat = @("EOF")
[core]
nick = leftover
host = chat.intergalactic.fm
use_ssl = false
port = 6667
owner = dreamer
channels = #town-square
enable = shoutout,version,mattermostconnect
nick_blocks = steve,montagu
host_blocks = 
prefix = \~
home = /home/user/src/streaming-media-stuff/sopel
#log_raw = True
#logging_level = DEBUG
#logdir = /home/user/src/streaming-media-stuff/sopel/sopel-ifm-log/

[clock]
tz = Europe/Amsterdam
time_format = %Y-%m-%d - %T%Z


[mattermost]
username = 'leftover'
password = 'BjXoHvJ5GK'
email = 'hmsa@eduif.nl'

[shout]
db = /home/user/src/streaming-media-stuff/leftover/leftover.db

[mastodon]
access_token = /home/user/src/streaming-media-stuff/leftover/mast/pytooter_usercred.secret
api_base_url = https://social.weho.st
EOF

  file{ "/home/${user}/.puppet/sopel_ifm.cfg":
    content => $sopel_indy,
  }


  put::userunit { 'sopel_leftover_ifm':
    command     => "/usr/local/bin/sopel -c /home/${user}/.puppet/sopel_ifm.cfg",
    user        => 'user',
    autorestart => true,
  autostart     => true,

  }

  $sopel_hackint = @("EOF")
[core]
nick = bot_${ircprefix}_${hostname}
#host = irc.indymedia.org
host = irc.hackint.org
use_ssl = true
port = 6697
owner = nobody
channels = #papillon
#logdir=/var/log/sopel
#pid_dir=/run/sopel
homedir=/home/user/src/streaming-media-stuff/sopel
enable = shoutout,version,mattermostconnect


[url]
exclude = 
exclusion_char = !

[admin]
hold_ground = true
auto_accept_invite = true

[safety]
enabled_by_default = false
known_good = 

[clock]
tz = UTC
time_format = %Y-%m-%d - %T%Z

[shout]
db = /home/user/src/streaming-media-stuff/leftover/leftover.db

[mastodon]
access_token = /home/user/src/streaming-media-stuff/leftover/mast/pytooter_usercred.secret
api_base_url = https://social.weho.st

[mattermost]
username = test
password = test
EOF

  file{ "/home/${user}/.puppet/sopel_hackint.cfg":
    content => $sopel_hackint,
  }

  if $enable {
    put::userunit { 'sopel_leftover':
      command     => "/usr/local/bin/sopel -c /home/${user}/.puppet/sopel_hackint.cfg",
      user        => 'user',
      autorestart => true,
      autostart   => true,
    }
    put::userunit { 'shoutloop':
      command     => '/home/user/src/streaming-media-stuff/leftover/shoutloop',
      user        => 'user',
      autorestart => true,
    autostart     => true,

    }
    put::userunit { 'sopel_leftover_indy':
      command     => '/usr/local/bin/sopel -c /home/user/.puppet/sopel_indymedia.cfg',
      user        => 'user',
      autorestart => true,
    }
  }

}
