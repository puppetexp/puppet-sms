# not very relevant anymore, except maybe to split up audio, so non audio stuff goes here
class sms::radio {

  include sms::software

  $pkgs = [
    'ffmpeg',
    'python3-venv',
    #'python-venv',
    'python-pip',
    'python3-pip',
    'liquidsoap',
    'ardour',
    'qjackctl',
    'hexchat',
    'linux-image-rt-amd64',
  ]

#include django

  ensure_packages($pkgs, {ensure => 'installed'})

  $py2pkgs = [
    'superlance',
  ]
  ensure_packages( $py2pkgs,
  {
    ensure   => present,
    provider => 'pip',
  })



  Exec {
    user    => 'root',
    cwd     => '/root/',
    timeout => 9999,
    path    => '/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin',
    environment => ['HOME=/root' ]
  }

}
