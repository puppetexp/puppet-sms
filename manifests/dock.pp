# peertube
class sms::dock (
  String  $glwritetoken = 'no_token_set',
  Boolean $dev          = false,
  )
  {

  # TODO:
  # * vpn for outgoing traffic (following other instances will make request to them)
  # * proxy in front
  Exec {
    user    => 'root',
    cwd     => '/root/',
    timeout => 9999,
    path    => '/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin',
    environment => ['HOME=/root' ]
  }

  firewall { '99 nginx port 80':
    dport  => '80',
    proto  => 'tcp',
    action => 'accept',
  }
  firewall { '99 nginx port 443':
    dport  => '443',
    proto  => 'tcp',
    action => 'accept',
  }
  firewall { '99 nginx backend port 8443':
    dport  => '8443',
    proto  => 'tcp',
    action => 'accept',
  }
  firewall { '99 nginx backend port 8080':
    dport  => '8080',
    proto  => 'tcp',
    action => 'accept',
  }
  firewall { '99 apache backend port 9443':
    dport  => '9443',
    proto  => 'tcp',
    action => 'accept',
  }
  firewall { '99 apache backend port 9080':
    dport  => '9080',
    proto  => 'tcp',
    action => 'accept',
  }
  firewall { '99 apache cache port 3443':
    dport  => '3443',
    proto  => 'tcp',
    action => 'accept',
  }
  firewall { '99 apache cache 3080':
    dport  => '3080',
    proto  => 'tcp',
    action => 'accept',
  }
  firewall { '99 1935':
    dport  => '1935',
    proto  => 'tcp',
    action => 'accept',
  }
  firewall { '99 8051':
    dport  => '8051',
    proto  => 'tcp',
    action => 'accept',
  }
  firewall { '99 8052':
    dport  => '8052',
    proto  => 'tcp',
    action => 'accept',
  }






}
