# Builds ardour from source
class sms::ardour6 (
  String $user = 'user',
  String $userdir = '/home/user',
  )
  {
  tag 'sms'
  require put::buster



# FIXME: times out, when doing make
  put::buildfromgit { 'ardour':
    repo            => 'https://github.com/Ardour/ardour.git',
    commands        => ['waf configure', 'waf'],
#    # FIXME: build times out (puppet run, not command)
#    commands        => ['waf configure'],
    debianbuilddeps => ['ardour'],
    debiandeps => ['libboost1.67-dev', 'libboost1.67-all-dev', 'libglibmm-2.4-dev', ]
  }



}

