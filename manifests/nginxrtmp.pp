# nginxrtmp
class sms::nginxrtmp (
  String  $hostname = 'testpeertube.laglab.org',
)
{


  # https://github.com/Nesseref/nginx-rtmp-auth
  # https://github.com/Nesseref/html5-livestreaming

  $pkgs = [
    'libnginx-mod-rtmp',
  ]
  ensure_packages( $pkgs,
  {
    ensure   => present,
    provider => 'apt',
  })
  file { '/srv/rtmprec/':
    ensure => directory,
  }


  file { '/srv/rtmprec/inputrec':
    ensure => directory,
  }


  file { '/srv/rtmprec/streams':
    ensure => directory,
  }



  # FIXME: the stuff that makes nginx read this dir is in hiera (common.yaml)
  file { '/etc/nginx/modconf.d/':
    ensure => directory,
  }

  $nginxrtmp_conf = {
    hostname    => $hostname,
  }
  file { "/etc/nginx/sites-enabled/rtmp.conf":
    content => epp("${module_name}/nginx-rtmp-minimal.conf.epp", $nginxrtmp_conf),
    notify => Service['nginx'],
  }
  file { '/etc/nginx/modconf.d/51-mod-rtmp-conf.conf':
    content => epp("${module_name}/nginx-rtmp.conf.epp", $nginxrtmp_conf),
    notify  => Service['nginx'],
  }




}
