# video streaming with obs and more
class sms::video {
  tag 'sms'

  require put::buster


  Exec {
    user    => 'root',
    cwd     => '/root/',
    timeout => 9999,
    path    => '/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin',
    environment => ['HOME=/root' ]
  }

  exec{ 'purge stuff we install from source':
    command => '/usr/bin/apt-get -y purge obs-studio'
  }




  $pkgs = [
    'ibniz',
    'lives',
    'lives-plugins',
    'ffmpeg',
    'build-essential',
    'checkinstall',
    'cmake',
    'libasound2-dev',
    'libavcodec-dev',
    'libavdevice-dev',
    'libavfilter-dev',
    'libavformat-dev',
    'libavutil-dev',
    'libcurl4-openssl-dev',
    'libfdk-aac-dev',
    'libfontconfig-dev',
    'libfreetype6-dev',
    'libgl1-mesa-dev',
    'libjack-jackd2-dev',
    'libjansson-dev',
    'libluajit-5.1-dev',
    'libpulse-dev',
    'libqt5x11extras5-dev',
    'libqt5multimedia5',
    'lua5.3',
    'libspeexdsp-dev',
    'libswresample-dev',
    'libswscale-dev',
    'libudev-dev',
    'libv4l-dev',
    'libvlc-dev',
    'libx11-dev',
    'libx264-dev',
    'libxcb-shm0-dev',
    'libxcb-xinerama0-dev',
    'libxcomposite-dev',
    'libxinerama-dev',
    'pkg-config',
    'python3-dev',
    'qtbase5-dev',
    'libqt5svg5-dev',
    'swig',
    'qjackctl',
    'ardour',
    'swh-lv2',
    'tap-plugins',
    'fil-plugins',
    'calf-plugins',
    'x42-plugins',
    #'liquidsoap-plugin-jack',
    'jack-tools',
    'ninja-build',
    'meson',
    'libgstreamer1.0-dev',
    'libgstreamer-plugins-bad1.0-dev',
    'libgstreamer-plugins-base1.0-dev',
    'libgstrtspserver-1.0-dev',
    'gstreamer1.0-plugins-good',
    'gstreamer1.0-plugins-ugly',
    'libqt5gstreamerutils-1.0-0',
    'gstreamer1.0-tools',
    'libass-dev',
    'libfreetype6-dev',
    'libsdl2-dev',
    'libtool',
    'libva-dev',
    'libvdpau-dev',
    'libvorbis-dev',
    'libxcb1-dev',
    'libxcb-shm0-dev',
    'libxcb-xfixes0-dev',
    'pkg-config',
    'texinfo',
    'wget',
    'zlib1g-dev',
    'nasm',
    'libx264-dev',
    'libx265-dev',
    'libnuma-dev',
    'libvpx-dev',
    'libfdk-aac-dev',
    'libmp3lame-dev',
    'libopus-dev',
    'avahi-utils',
    'libgstreamer1.0-dev',
    'libgstreamer-plugins-base1.0-dev',
    'gstreamer1.0-plugins-base',
    'gstreamer1.0-plugins-good',
    'gstreamer1.0-plugins-bad',
    'gstreamer1.0-plugins-ugly',
    'gstreamer1.0-libav',
    'libgstrtspserver-1.0-dev',
    'cargo',
  #nageru
    'libqt5printsupport5',
    'libsdl2-image-dev',
    'libbmusb-dev',
    'libbmusb5',
    'libeigen3-dev',
    'libepoxy-dev',
    'libgcrypt20-dev',
    'libgmp-dev',
    'libgmpxx4ldbl',
    'libgnutls-dane0',
    'libgnutls-openssl27',
    'libgnutls28-dev',
    'libgnutlsxx28',
    'libgpg-error-dev',
    'libidn11-dev',
    'liblua5.2-0',
    'liblua5.2-dev',
    'liblua5.3-dev',
    'liblua5.1-dev',
    'liblua5.1-0-dev',
    'liblua50-dev',
    'libmicrohttpd-dev',
    'libmicrohttpd12',
    'libmovit-dev',
    'libmovit8',
    'libp11-kit-dev',
    'libpci-dev',
    'libprotobuf-dev',
    'libprotobuf-lite17',
    'libprotobuf17',
    'libprotoc17',
    'libreadline-dev',
    'libtasn1-6-dev',
    'libunbound8',
    'libunbound-dev',
    'libzita-resampler-dev',
    'libzita-resampler1',
    'nettle-dev',
    'protobuf-compiler',
    'googletest',
    'libgtest-dev',
    'libjpeg62-turbo-dev',
    'libjpeg-dev',
    'libsqlite3-dev',
    'libqcustomplot-dev',
    #'nageru',
    #'futatabi',
    #editing:
    'kdenlive',
    'ebumeter',
    'python-pip',
    'python-pip',
    'libbmusb-dev',
    'libbmusb5',
    'qt5-default',
    'libusb-1.0-0-dev',
    'libusb-1.0-doc'

  ]

  include put::nodejs

  ensure_packages($pkgs, {ensure => 'installed'})


  package { 'obs-websocket-py-pip3':
    ensure   => present,
    name     => 'obs-websocket-py',
    provider => 'pip3',
  }
  package { 'obs-wws-rc-pip3':
    ensure   => present,
    name     => 'obs-ws-rc',
    provider => 'pip3',
  }





#  exec { 'backports meson':
#    command => 'apt-get -t stretch-backports install meson',
#    cwd     => "${srcdir}/"
#  }


  $srcdir = '/usr/local/src'
  #49152 to 65535.

  firewall { '121 obs websocket for remote control':
    dport  => '4444',
    proto  => 'tcp',
    action => 'accept',
  }


  firewall { '123 ndi streams tcp':
    dport  => '49152-65535',
    proto  => 'tcp',
    action => 'accept',
  }

  firewall { '123 ndi streams udp':
    dport  => '49152-65535',
    proto  => 'udp',
    action => 'accept',
  }




}

