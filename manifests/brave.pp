# sms brave
class sms::brave (
  String  $glwritetoken = 'no_token_set',
  String $user = 'user',
  )
  {
  tag 'sms'

  include git

  $bravepkgs = [
    'gcc',
    'gir1.2-gst-plugins-bad-1.0',
    'gobject-introspection',
    'gstreamer1.0-libav',
    'gstreamer1.0-nice',
    'gstreamer1.0-plugins-bad',
    'gstreamer1.0-plugins-base',
    'gstreamer1.0-plugins-good',
    'gstreamer1.0-plugins-ugly',
    'gstreamer1.0-tools',
    'libcairo2-dev',
    'libcairo2-dev',
    'libgirepository1.0-dev',
    'pkg-config',
    'python-gst-1.0',
    'python3-dev',
    'python3-pip',
    'pipenv',
  ]


  #ensure_packages($pkgs, {ensure => 'installed'})

  file { '/home/brave':
    ensure => directory,
    owner  => 'brave',
  }

  user { 'brave':
    system => true,
    home   => '/home/brave',
  }
  -> put::buildfromgit { 'brave':
    repo       => 'https://github.com/bbc/brave.git',
    user       => 'brave',
    commands   => ['pipenv install'],
    #debianbuilddeps => ['ardour'],
    debiandeps => $bravepkgs,
  }

  put::unit { 'brave':
    command => 'pipenv run ./brave.py',
    user    => 'brave',
  }

}
