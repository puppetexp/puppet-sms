# peertube cli
class sms::peertubecli (
  String $user = 'user',
)
{

#    git clone https://github.com/Chocobozzz/PeerTube.git
#    5  NOCLIENT=1 yarn install --pure-lockfile
  include put::nodejs

  file { '/usr/local/bin/peertube_upload.sh':
    source => "puppet:///modules/${module_name}/peertube_upload.sh",
    mode   => '0755',
  }

  $nodepkgs = [
    'yarn',
    'typescript',
    'supertest',
    'chai'
  ]
  ensure_packages( $nodepkgs,
  {
    ensure   => latest,
    provider => 'npm',
    install_options => [ '-g' ],
  })


  put::buildfromgit { 'peertubecli':
    repo     => 'https://github.com/Chocobozzz/PeerTube.git',
    user     => $user,
    commands => ['yarn install --pure-lockfile'],
    env      => ['NOCLIENT=1'],
  }



}
