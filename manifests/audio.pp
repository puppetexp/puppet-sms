# sms audio
class sms::audio (
  String  $glwritetoken = 'no_token_set',
  String  $ircprefix  = 'irc-prefix-not-set',
  String  $jack_cmdline = '-d dummy',
  String $user = 'user',
  String $userdir = '/home/user',
  String $liqfile = 'setafileifyouwantliquidsoap.liq',
  # server/room
  String $mumbleserver = 'farma.cisti.org/test',
  Boolean $video = false,
  Boolean $netmaster = false,

  )
  {
  tag 'sms'
  require put::buster
  require sms::v4l2sink

  put::userunit { 'liquidsoap':
    command   => "/usr/bin/liquidsoap /home/user/src/streaming-media-stuff/liquidsoap/${liqfile}",
    user      => 'user',
    autostart => false,
    after     => [ 'jack.service' ],
    wants     => [ 'jack.service' ],
  }

  file{ "${userdir}/.config/Mumble/":
    ensure => directory,
    owner  => 'user'
  }

  $mumblerc = @("EOF")
[General]
lastupdate=2

[jack]
output=2 # or 2 (mono/stereo)
autoconnect=false # or false
startserver=false # or false

[audio]
input=jack
output=jack
quality=16000

[messages]
size=29

[messagesounds]
size=29
EOF

  file{ "${userdir}/.config/Mumble/Mumble.conf.jack":
    ensure  => present,
    content => $mumblerc,
    #  notify  => Put::Userunit['hexchat']
  }

  put::userunit { 'mumble_live':
    command     => "/usr/local/src/mumble/build/mumble -jn mumble-live -m mumble://${ircprefix}_${::hostname}_live@${mumbleserver}/live",
    tag         => ['mumble'],
    user        => 'user',
    autostart   => false,
    autorestart => true,
    after       => [ 'jack.service', 'xsession.target'],
    wants       => [ 'jack.service', 'xsession.target'],
    partof      => [ 'xsession.target' ],
    wantedby    => [ 'xsession.target' ],
  }
  put::userunit { 'mumble_talkback':
    command     => "/usr/local/src/mumble/build/mumble -jn mumble-talkback -m mumble://${ircprefix}_${::hostname}_talkback@${mumbleserver}/talkback",
    tag         => ['mumble'],
    user        => 'user',
    autostart   => false,
    autorestart => true,
    after       => [ 'jack.service', 'xsession.target'],
    wants       => [ 'jack.service', 'xsession.target'],
    partof      => [ 'xsession.target' ],
    wantedby    => [ 'xsession.target' ],
  }




  #include sms::kxstudio
  include put::xfdesktop
  include put::scripts
  include sms::software
  include sms::audiosoftware
  include sms::bot
  #include sms::videodriver
  #include sms::peertubecli



  #include acme

  #::acme::certificate { "${hostname}testcertinaudio.s.laglab.org":
  #  use_profile    => 'bprouter_anotherprofile',
  #  use_account    => 'ssltest@laglab.org',
  #  letsencrypt_ca => 'staging',
  #  acme_host      => 'puppet.lag',
  #}

  #firewall { '000 accept all icmp':
  #  proto  => 'icmp',
  #  action => 'accept',
  #}

  firewall { '991 jack net':
    dport  => '19000',
    proto  => 'udp',
    action => 'accept',
  }
  firewall { '991 avahi udp':
    dport  => '5353',
    proto  => 'udp',
    action => 'accept',
  }
  firewall { '992 avahi tcp':
    dport  => '5353',
    proto  => 'tcp',
    action => 'accept',
  }

  firewall { '991 avahi udp 60774' :
    dport  => '60774',
    proto  => 'udp',
    action => 'accept',
  }
  firewall { '992 avahi tcp 60774':
    dport  => '60774',
    proto  => 'tcp',
    action => 'accept',
  }


  firewall { '993 pulse':
    dport  => '4317',
    proto  => 'tcp',
    action => 'accept',
  }
  firewall { '993 pulse udp':
    dport  => '4317',
    proto  => 'udp',
    action => 'accept',
  }
  firewall { '993 pulse 4713':
    dport  => '4713',
    proto  => 'tcp',
    action => 'accept',
  }
  firewall { '993 pulse udp 4713':
    dport  => '4713',
    proto  => 'udp',
    action => 'accept',
  }







  $srcdir = '/root/src'


  $pkgs = [

    'x11vnc',
#deps
    'rsync',


#kernel
  'linux-image-4.19.0-11-rt-amd64',
  'linux-headers-4.19.0-11-rt-amd64',


  'mesa-utils',
#etc
  'hexchat',
  'aj-snapshot',
#hexchat

# ardour build-dep
#  autopoint cdbs debhelper dh-autoreconf dh-buildinfo dh-strip-nondeterminism dwz gettext gir1.2-gtk-2.0 gir1.2-harfbuzz-0.0 intltool intltool-debian itstool jackd1
#  ladspa-sdk libarchive-dev libarchive-zip-perl libasound2-dev libatk1.0-dev libatkmm-1.6-dev libatlas3-base libaubio-dev libaubio5 libbluetooth-dev libboost-dev
#  libboost1.67-dev libcairomm-1.0-dev libcurl4-gnutls-dev libcwiid-dev libcwiid1 libdebhelper-perl libfftw3-bin libfftw3-dev libfftw3-long3 libfftw3-quad3
#  libfile-stripnondeterminism-perl libflac-dev libfluidsynth-dev libfribidi-dev libglibmm-2.4-dev libgraphite2-dev libgtk2.0-dev libgtkmm-2.4-dev libharfbuzz-dev
#  libharfbuzz-gobject0 libhidapi-dev libhidapi-libusb0 libjack-dev libjack0 liblilv-dev liblo-dev liblrdf0 liblrdf0-dev libltc-dev libogg-dev libpango1.0-dev
#  libpangomm-1.4-dev libqm-dsp-dev libqm-dsp0 libraptor2-0 libraptor2-dev librubberband-dev libsamplerate0-dev libserd-dev libsigc++-2.0-dev libsndfile1-dev
#  libsord-dev libsratom-dev libsuil-0-0 libsuil-dev libtag1-dev libusb-1.0-0-dev libvamp-hostsdk3v5 libvamp-sdk2v5 libvorbis-dev libxcomposite-dev libxcursor-dev
#  libxft-dev libxi-dev libxinerama-dev libxml2-utils libxrandr-dev libxslt1-dev libyajl-dev lv2-dev pango1.0-tools po-debconf python-isodate python-libxml2
#  python-pyparsing python-rdflib vamp-plugin-sdk x11proto-composite-dev x11proto-input-dev x11proto-randr-dev x11proto-xinerama-dev
  ]



  Exec {
    user    => 'root',
    cwd     => '/root/',
    timeout => 9999,
    path    => '/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin',
    environment => ['HOME=/root' ]
  }

  #FIXME: otherwise no dbus when user is not logged in???
  exec { 'enable lingering':
    command => 'loginctl enable-linger user',
  }

  ensure_packages($pkgs, {ensure => 'installed'})

  package { 'linux-image-5.2.0-0.bpo.2-amd64':
    ensure => absent
  }
  package { 'linux-image-4.19.0-11-amd64':
    ensure => absent
  }
  package { 'linux-image-4.19.0-12-amd64':
    ensure => absent
  }
 
  package { 'linux-image-4.19.0-12-rt-amd64':
    ensure => absent
  } 
  #package { 'linux-image-5.2.0-0.bpo.3-amd64':
  #  ensure => absent
  #}


  #FIXME: this is because of a bug in liquidsoap (crashes when loading 1 of the plugins)
  package{ 'frei0r-plugins':
    ensure => absent,
  }

  file { '/var/lib/systemd/linger/':
    ensure => directory,
    #owner  => 'user',
  }
  file { "/var/lib/systemd/linger/${user}":
    ensure => present,
    #owner  => 'user',
  }



  file { "${userdir}/src/":
    ensure => directory,
    owner  => 'user',
  }

  vcsrepo { '/home/user/src/leftover-website':
    ensure   => present,
    provider => git,
    owner    => $user,
    group    => $user,
    remote   => 'origin',
    user     => $user,
    #FIXME: keep_local_changes does not work
    #keep_local_changes => true,
    source   => {
      'origin' => 'https://gitlab+deploy-token-25:iSuu4Z-u3KnVrtMCTM53@git.puscii.nl/admradio/pap-website-jekyll',
      'write'  => "https://oauth2:${glwritetoken}@git.puscii.nl/admradio/pap-website-jekyll.git",
    }
  }


  vcsrepo { '/home/user/src/streaming-media-stuff':
    ensure   => present,
    provider => git,
    owner    => $user,
    group    => $user,
    remote   => 'origin',
    revision => 'internet',
    user     => $user,
    #FIXME: keep_local_changes does not work
    #keep_local_changes => true,
    source   => {
      'origin' => 'https://git.puscii.nl/sms/streaming-media-stuff.git',
      'write'  => "https://oauth2:${glwritetoken}@git.puscii.nl/sms/streaming-media-stuff.git",

#      'gl'     => 'git@185.52.224.4:sms/streaming-media-stuff.git'
    }
  }

  vcsrepo { "/home/${user}/.config/obs-studio":
    ensure   => present,
    provider => git,
    owner    => $user,
    group    => $user,
    revision => 'internet',
    remote   => 'origin',
    user     => $user,
    #keep_local_changes => true,
    source   => {
      'origin' => 'https://gitlab+deploy-token-19:fM37gTvVHQ417P3hHy4w@git.puscii.nl/sms/obs.git',
      'write'  => "https://oauth2:${glwritetoken}@git.puscii.nl/sms/obs.git",
    }
  }

  #FIXME: turn everything this does in puppet thingies
  vcsrepo { "${userdir}/dot-config":
    ensure   => present,
    provider => git,
    owner    => $user,
    group    => $user,
    user     => $user,
    remote   => 'origin',
    #keep_local_changes => true,
    source   => {
      'origin' => 'https://gitlab+deploy-token-22:ifzspBMz5GbzxzL2PESV@git.puscii.nl/sms/dot-config.git',
      'write'  => "https://oauth2:${glwritetoken}@git.puscii.nl/sms/dot-config.git",
    }

  }
#  -> exec { 'rsync .config':
#      command => "rsync -a ${userdir}/dot-config ${userdir}/.config",
#      path    => '/usr/local/bin:/usr/bin:/bin',
#  #    require => Vcsrepo[$djdir],
#  }

  #do not start pasystray, it makes messages appear all the time
  file { '/etc/xdg/autostart/pasystray.desktop':
    ensure => absent,
  }


  file { "${userdir}/media":
    ensure => directory,
    owner  => 'user',

  }


  file { "${userdir}/media/ardour":
    ensure => directory,
    owner  => $user,

  }

  vcsrepo { "${userdir}/media/ardour":
    ensure   => present,
    provider => git,
    owner    => $user,
    remote   => 'origin',
    user     => $user,
    source   => {
      'origin' => 'https://git.puscii.nl/sms/ardour.git',
      'write'  => "https://oauth2:${glwritetoken}@git.puscii.nl/sms/ardour.git",
    }

  }

  file{ "${userdir}/.config/hexchat/":
    ensure => directory,
    owner  => 'user'
  }


  $hexchatrc = @("EOF")

#hexchat
version = 2.12.4
irc_logging = 0
gui_slist_skip = 1
irc_nick1 = ${ircprefix}_${::hostname}
irc_nick2 = ${ircprefix}_${::hostname}_1
irc_nick3 = ${ircprefix}_${::hostname}_2
irc_nick_hilight =
irc_no_hilight = NickServ,ChanServ,InfoServ,N,Q
EOF

  file{ "${userdir}/.config/hexchat/hexchat.conf":
    content => $hexchatrc,
    notify  => Put::Userunit['hexchat']
  }
#FIXME: intergalatic needs autosendcmd, and no ssl
  $hexchatservlist = @("EOF")
N=intergalactic
E=UTF-8 (Unicode)
F=127
D=0
S=chat.intergalactic.fm/6667
J=#town-square


N=hackint
E=UTF-8 (Unicode)
F=127
D=0
S=irc.hackint.org/6697
J=#papillon

N=indymedia
E=UTF-8 (Unicode)
F=127
D=0
S=irc.indymedia.org/6697
J=#papillon
J=#lag

N=IRCNet
E=UTF-8 (Unicode)
F=19
D=0
S=open.ircnet.net

N=OFTC
E=UTF-8 (Unicode)
F=19
D=0
S=irc.oftc.net

N=freenode
L=6
E=UTF-8 (Unicode)
F=23
D=0
S=chat.freenode.net
S=irc.freenode.net
EOF

  file{ "${userdir}/.config/hexchat/servlist.conf":
    content => $hexchatservlist,
    notify  => Put::Userunit['hexchat']
  }

  $vlcrc = @("EOF")
[core]
aout=jack

[jack] # JACK audio output
jack-auto-connect=1
jack-connect-regex=system
jack-name=vlc
jack-gain=1.000000


EOF

  file { "/home/${user}/.config/vlc":
    ensure => directory,
  }

  file { "/home/${user}/.config/vlc/vlcrc":
    content => $vlcrc,
  }




  put::userunit { 'hexchat':
    command     => '/usr/bin/hexchat --minimize=2',
    user        => 'user',
    autostart   => false,
    autorestart => true,
    after       => [ 'xsession.target' ],
    wants       => [ 'xsession.target' ],
    partof      => [ 'xsession.target' ],
    wantedby    => [ 'xsession.target' ],
  }

  $genmon_conf = @("EOF")
Command=/home/user/src/streaming-media-stuff/panelplugin/test.sh
UseLabel=0
Text=(genmon)
UpdatePeriod=2000
Font=Sans 10
EOF

#  file { "/home/${user}/.config/xfce4":
#    ensure => directory,
#  }

#  file { "/home/${user}/.config/xfce4/panel":
#    ensure => directory,
#  }

#  file { "/home/${user}/.config/xfce4/panel/genmon-2.rc":
#    content => $genmon_conf,
#  }
#  file { "/home/${user}/.config/xfce4/panel/genmon.rc":
#    content => $genmon_conf,
#  }





  $mocp_conf = @("EOF")
MusicDir = /home/user/Nextcloud
StartInMusicDir = yes
ShowStreamErrors = yes
Repeat = no
Shuffle = no
AutoNext = yes
InputBuffer = 512                  # Minimum value is 32KB
OutputBuffer = 512                 # Minimum value is 128KB
Prebuffering = 64
SoundDriver JACK
JackClientName = "moc"
JackStartServer = no
JackOutLeft = "ardour:MOC/audio_in 1"
JackOutRight = "ardour:MOC/audio_in 1"
Softmixer_SaveState = no 
Equalizer_SaveState = no
ShowTime = IfAvailable
Precache = yes
SavePlaylist = yes
SyncPlaylist = yes
SeekTime = 1
SilentSeekTime = 15
UseRealtimePriority = no
EOF



  $limits_conf = @("EOF")

@audio   -  rtprio     95
@audio   -  memlock    unlimited
@audio   -  nice      -19

EOF

  file { '/etc/security/limits.d/audio.conf':
    content => $limits_conf,
  }

$pa_default = @(EOF)
#!/usr/bin/pulseaudio -nF

.fail

#load-module module-device-restore
#load-module module-stream-restore
#load-module module-card-restore

#load-module module-augment-properties

#load-module module-switch-on-port-available

load-module module-native-protocol-unix

### Network access (may be configured with paprefs, so leave this commented
### here if you plan to use paprefs)
#load-module module-esound-protocol-tcp
#load-module module-native-protocol-tcp

### Load the RTP receiver module (also configured via paprefs, see above)
#load-module module-rtp-recv

### Load the RTP sender module (also configured via paprefs, see above)
#load-module module-null-sink sink_name=rtp format=s16be channels=2 rate=44100 sink_properties="device.description='RTP Multicast Sink'"
#load-module module-rtp-send source=rtp.monitor

#load-module module-default-device-restore
#load-module module-rescue-streams
#load-module module-always-sink
#load-module module-intended-roles

### If autoexit on idle is enabled we want to make sure we only quit
### when no local session needs us anymore.
.ifexists module-console-kit.so
load-module module-console-kit
.endif
.ifexists module-systemd-login.so
load-module module-systemd-login
.endif

### Enable positioned event sounds
#load-module module-position-event-sounds

### Cork music/video streams when a phone stream is active
#load-module module-role-cork
#load-module module-filter-heuristics
#load-module module-filter-apply

#load-module module-switch-on-port-available
load-module module-rtp-recv
load-module module-native-protocol-tcp auth-ip-acl=127.0.0.1;10.205.12.0/24 auth-anonymous=1
load-module module-zeroconf-publish
load-module module-zeroconf-discover
load-module module-jack-sink channels=2
load-module module-jack-source channels=2
#load-module module-alsa-sink device=hw:0,0
set-default-sink jack_out
set-default-source jack_in

EOF

file { '/etc/pulse/default.pa':
    content => $pa_default,
  }


$jack_plumbing = @(EOF)

## bunch of mediaplayers
(disconnect "moc:output(.*)" "system:playback_(.*)")
#(connect-exclusive "moc:output0" "ardour:MOC/audio_in 1")
#(connect-exclusive "moc:output1" "ardour:MOC/audio_in 2")
(connect "moc:output0" "ardour:MOC/audio_in 1")
(connect "moc:output1" "ardour:MOC/audio_in 2")
(connect "mpv:out_0" "ardour:MPV/audio_in 1")
(connect "mpv:out_1" "ardour:MPV/audio_in 2")
#FIXME vlc
(disconnect "mpv:out_0" "system:playback_1")
(disconnect "mpv:out_1" "system:playback_2")
(disconnect "Mixxx:out_0" "system:playback_1"
(disconnect "Mixxx:out_1" "system:playback_2")
(connect "Mixxx:out_0" "ardour:MIXXX/audio_in 1")
(connect "Mixxx:out_1" "ardour:MIXXX/audio_in 2")
(connect "system:capture_1" "jack-scope-*:in_1")
(connect "system:capture_2" "jack-scope-*:in_2")
(connect "Music Player Daemon:left" "ardour:MPD/audio_in 1")
(connect "Music Player Daemon:right" "ardour:MPD/audio_in 2")
(disconnect "Music Player Daemon:left" "system:playback_1")
(disconnect "Music Player Daemon:right" "system:playback_2")
 
# pulseaudio (to be able to use videoconferencing stuff in webbrowser / play songs from website)
(disconnect "PulseAudio JACK Sink:front-left" "system:playback_1")
(disconnect "PulseAudio JACK Sink:front-right" "system:playback_2")
(connect "PulseAudio JACK Sink:front-left" "ardour:PULSE/audio_in 1")
(connect "PulseAudio JACK Sink:front-right" "ardour:PULSE/audio_in 2")
            
#(connect "mumble:output" "ardour:mumble/audio_in 1")
#(connect "ardour:Master/audio_out 1" "mumble:input")

## connect pulse mic in to talkback mic, so you can use webrtc shit (like meet.jit.si)
(connect "ardour:TBMIC/audio_out 1" "PulseAudio JACK Source:front-left"  )
(connect "ardour:TBMIC/audio_out 2" "PulseAudio JACK Source:front-right"  )


# MUMBLE talkback: connected to talkback mic chan, and talkback channel in ardourk
(connect "ardour:TBMIC/audio_out 1" "mumble-talkback:input")
(connect "mumble-talkback:output_1" "ardour:MUM_TALKBACK/audio_in 1")

#MUMBLE live: connected to master out / MUM-live
(connect "ardour:Master/audio_out 1" "mumble-live:input")
(connect "mumble-live:output_1" "ardour:MUM_LIVE/audio_in 1")

(connect "ardour:Master/audio_out 1" "darkice:left")
(connect "ardour:Master/audio_out 2" "darkice:right")

#various possible streaming outputs
(connect "ardour:Master/audio_out 1" "liquidsoap-ls1ls2:in_0")
(connect "ardour:Master/audio_out 2" "liquidsoap-ls1ls2:in_1")
(connect "ardour:Master/audio_out 1" "liquidsoap-ls1ls2-low:in_0")
(connect "ardour:Master/audio_out 2" "liquidsoap-ls1ls2-low:in_1")
(connect "ardour:Master/audio_out 1" "liquidsoap-ls1ls2-verylow:in_0")
(connect "ardour:Master/audio_out 2" "liquidsoap-ls1ls2-verylow:in_1")
(connect "ardour:Master/audio_out 1" "liquidsoap-location:in_0")
(connect "ardour:Master/audio_out 2" "liquidsoap-location:in_1")

(connect "ardour:Master/audio_out 1" "liquidsoap-location-low:in_0")
(connect "ardour:Master/audio_out 2" "liquidsoap-location-low:in_1")
(connect "ardour:Master/audio_out 1" "liquidsoap-location-verylow:in_0")
(connect "ardour:Master/audio_out 2" "liquidsoap-location-verylow:in_1")
(connect "ardour:Master/audio_out 1" "obs-input:in_1")
(connect "ardour:Master/audio_out 2" "obs-input:in_2")


(connect "ardour:Master/audio_out 1" "liquidsoap-papillon-studio:in_0")
(connect "ardour:Master/audio_out 2" "liquidsoap-papillon-studio:in_1")

#hardware input channels, both mapped in stereo and mono
(connect "system:capture_1" "ardour:STEREO1/audio_in 1")
(connect "system:capture_2" "ardour:STEREO1/audio_in 2")
(connect "system:capture_3" "ardour:STEREO2/audio_in 1")
(connect "system:capture_4" "ardour:STEREO2/audio_in 2")

(connect "system:capture_1" "ardour:MONO1/audio_in 1")
(connect "system:capture_2" "ardour:MONO2/audio_in 1")
(connect "system:capture_3" "ardour:MONO3/audio_in 1")
(connect "system:capture_4" "ardour:MONO4/audio_in 1")



EOF

  file { '/etc/jack-plumbing.conf':
    content => $jack_plumbing,
    notify  => Put::Userunit['jack-plumbing'],

  }

  put::userunit { 'jack-plumbing':
    command => '/usr/bin/jack-plumbing /etc/jack-plumbing.conf',
    user    => 'user',
    after   => [ 'jack.service' ],
    wants   => [ 'jack.service' ],


  }


  put::userunit { 'jack_plumbing':
    ensure  => absent,
    command => '/usr/bin/jack-plumbing /etc/jack-plumbing.conf',
    user    => 'user',
  }


$aj_snapshot = @(EOF)
<aj-snapshot>
<alsa>
  <client name="System">
    <port id="0" />
    <port id="1" />
  </client>
  <client name="Midi Through">
    <port id="0" />
  </client>
  <client name="a2jmidid" />
  <client name="ecasound">
    <port id="0" />
  </client>
</alsa>
<jack>
  <client name="a2j">
    <port name="Midi Through [14] (capture): Midi Through Port-0" />
    <port name="ecasound [129] (capture): ecasound" />
  </client>
</jack>
</aj-snapshot>
EOF

  file { '/etc/aj-snapshot.conf':
    content => $aj_snapshot,
    notify  => Put::Userunit['aj-snapshot'],

  }

  put::userunit { 'aj-snapshot':
    command => '/usr/bin/aj-snapshot --alsa -d /etc/aj-snapshot.conf',
    user    => 'user',
    after   => [ 'jack.service' ],
    wants   => [ 'jack.service' ],


  }




  put::userunit { 'jackd':
    ensure      => 'absent',
    command     => "/usr/bin/jackd ${jack_cmdline}",
    environment => ['JACK_NO_AUDIO_RESERVATION=1'],
    user        => 'user',
  }



  put::userunit { 'jack':
    command       => "/usr/bin/jackd ${jack_cmdline}",
    environment   => ['JACK_NO_AUDIO_RESERVATION=1'],
    user          => 'user',
    autostart     => true,
    autorestart   => true,
    execstartpre  => ['-/usr/bin/killall jackd', '-/usr/bin/killall jackdbus', '-/usr/bin/pactl unload-module module-alsa-card'],
    execstartpost => [
 	'-/usr/bin/pacmd load-module module-jack-source channels=2',
 	'-/usr/bin/pacmd load-module module-jack-sink channels=2', 
	'-/usr/bin/pacmd set-default-sink jack_out',
	'-/usr/bin/pacmd set-default-source jack_in'


],

    #execstartpost => ['-/usr/bin/pulseaudio --kill', '-/usr/bin/pulseaudio --start'],
  }
  if $netmaster {
    put::userunit { 'jack_netmaster':
      command => '/usr/bin/jack_load netmaster',
      user    => 'user',
      after   => [ 'jack.service' ],
      wants   => [ 'jack.service' ],
      partof  => [ 'jack.service'],
    }
  }

  put::userunit { 'killpulse':
    ensure => 'absent';
    #command     => '/home/user/src/streaming-media-stuff/scripts/killpulse.sh',
    #environment => ['JACK_NO_AUDIO_RESERVATION=1'],
    #user        => 'user',
    #autostart   => false,
    #autorestart => true,
    #after       => [ 'jack.service', 'xsession.target'],
    #wants       => [ 'jack.service', 'xsession.target'],
    #wantedby    => [ 'xsession.target' ],
  }

  put::userunit { 'xclock':
    ensure => 'absent';
    #command     => '/usr/bin/xclock',
    #environment => ['JACK_NO_AUDIO_RESERVATION=1'],
    #user        => 'user',
    #autostart   => false,
    #autorestart => true,
    #after       => [ 'jack.service', 'xsession.target'],
    #wants       => [ 'jack.service', 'xsession.target'],
    #partof      => [ 'xsession.target' ],
    #wantedby    => [ 'xsession.target' ],
  }

  put::userunit { 'radiotray':
    command     => '/home/user/src/streaming-media-stuff/panelplugin/tray.py',
    environment => ['JACK_NO_AUDIO_RESERVATION=1'],
    user        => 'user',
    autostart   => false,
    autorestart => true,
    after       => [ 'jack.service', 'xsession.target'],
    wants       => [ 'jack.service', 'xsession.target'],
    partof      => [ 'xsession.target' ],
    wantedby    => [ 'xsession.target' ],
  }

  put::userunit { 'x11vnc':
    command     => '/usr/bin/x11vnc -nopw -scale 1/2 -rfbport 5905 -shared -forever -loop -localhost',
    environment => ['JACK_NO_AUDIO_RESERVATION=1'],
    user        => 'user',
    autostart   => false,
    autorestart => true,
    after       => [ 'jack.service', 'xsession.target'],
    wants       => [ 'jack.service', 'xsession.target'],
    partof      => [ 'xsession.target' ],
    wantedby    => [ 'xsession.target' ],
  }





#  put::userunit { 'jackd':
#    command     => "/usr/bin/jackd ${jack_cmdline}",
#    environment => ['JACK_NO_AUDIO_RESERVATION=1'],
#    user        => 'user',
#  }

  put::userunit { 'ardour':
    command     => '/home/user/src/streaming-media-stuff/scripts/ardourfromtemplate.sh',
    user        => 'user',
    autostart   => false,
    autorestart => true,
    after       => [ 'jack.service', 'xsession.target' ],
    wants       => [ 'jack.service', 'xsession.target' ],
    partof      => [ 'xsession.target' ],
    wantedby    => [ 'xsession.target' ],


  }

  put::userunit { 'mocp':
    command   => '/usr/bin/mocp -F',
    user      => 'user',
    autostart => true,
    after     => [ 'jack.service' ],
    wants     => [ 'jack.service' ],


  }


#  if $video {
    put::userunit { 'obs':
      command     => '/usr/bin/obs',
      user        => 'user',
      ## check if it still works with autostart false
      autostart   => false,
      autorestart => true,
      after       => [ 'jack.service', 'xsession.target'],
      wants       => [ 'jack.service', 'xsession.target'],
      partof      => [ 'xsession.target' ],
      wantedby    => [ 'xsession.target' ],

    }

    #FIXME: install obs-tablet-remote this somewhere
    put::userunit {'obs-tablet-remote':
      command => '/usr/bin/npm run dev',
      cwd     => '/usr/local/src/obs-tablet-remote/',
      user    => 'user',
    }


#  }

### ffado
#cdbs libconfig++-dev libconfig-dev libconfig9 libdbus-c++-bin libdbus-c++-dev libglibmm-2.4-dev libiec61883-dev libsigc++-2.0-dev libxml++2.6-dev libxml2-dev
#  pyqt5-dev-tools

}
